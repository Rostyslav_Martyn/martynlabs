﻿import Foundation
func SymbolAnalyser( Symb: Character){
    print("---------------------------------")
    if Symb.isNumber {
        print("Число: \(Symb)")
    }
    if Symb.isLetter {
        print("Буква: \(Symb)")
        if Symb.isUppercase {
           print("Буква верхнього регістру")
        }else {
            print("Буква нижнього регістру")
        }
        let asciiIden = Symb.asciiValue
        if asciiIden != nil {
            
            for i in 0x0041...0x005a {
                if i == asciiIden! {
                    print("Англійського алфавіту")
                }
            }
            for i in 0x0061...0x007a {
                if i == asciiIden!{
                    print("Англійського алфавіту")
                }
            }
        } else {
                print("Російського алфавіту")
    }
    } else {
        print("Невідомий символ\(Symb)")
    }
}

var symbols = [Character]()
symbols.append(contentsOf: ["1", "s", "9", "Q", "и", "А", "2"])

for counter in symbols{
    SymbolAnalyser(Symb: counter)
}