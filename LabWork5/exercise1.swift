import Foundation
func factorial(_ num: Int) -> Int{
    if num < 0 { return 0 }
    if num == 0 { return 1 }
    if num == 1 { return 1 }
    if num > 1 {
        return num * factorial(num - 1)
    } else {
        return 0
    }
}
print(factorial(3))
print(factorial(4))
print(factorial(5))
print(factorial(10))