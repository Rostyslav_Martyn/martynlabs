


import Foundation

var integerNumber: Int?
var decimalNumber: Float?

integerNumber = 12
//integerNumber integerNumber
var count = integerNumber!
while count > 0 {
    integerNumber! += 1
    count -= 1
}
print("integer number = ", integerNumber ?? "empty")

integerNumber = -integerNumber!
print("sign changed integer number = ", integerNumber ?? "empty")

decimalNumber = Float(integerNumber!)
print("decimal number = ", decimalNumber!)

var pairOfValues: Any? = integerNumber
//var pairOfValues: Float? = Float(integerNumber != nil ? integerNumber! : 0) + (decimalNumber != nil ? decimalNumber! : 0)

if type(of: pairOfValues) == Optional<Float>.self {
    print("Value in pairOfValues is of type Float?")
} else if type(of: pairOfValues) == Optional<Int>.self {
    print("Value in pairOfValues is of type Int?")
}

print("PairOfValues equals ", pairOfValues != nil ? pairOfValues! : "Variable is empty!")

print("Decimal number equals ", decimalNumber != nil ? decimalNumber! : 0)
