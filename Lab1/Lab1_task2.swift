

import Foundation

var num1: UInt8 = 12
var num2: Int8 = -100
var num3hex: UInt8 = 0x0080
var num4 = Int16.min
var num5 = Int64.max
var num6: Float16 = 10235.34
var char_m: Character = "a"
var str_m: String = "Hello world"
var bl_m: Bool = true
var numtest = [12 : "twelve"]
