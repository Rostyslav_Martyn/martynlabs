


import Foundation

var i_let: String = "i"
var u_let: String = "u"

var str_line: String = "Hello World. This is Swift programming language"

var length: Int = Int(str_line.count)
print("Given line length is: ",length)
str_line = str_line.replacingOccurrences(of: i_let, with: u_let)

print(str_line)
str_line.remove(at: str_line.index(after: String.Index(utf16Offset: 3, in: str_line)))
str_line.remove(at: str_line.index(after: String.Index(utf16Offset: 6, in: str_line)))
str_line.remove(at: str_line.index(after: String.Index(utf16Offset: 9, in: str_line)))
print(str_line)
str_line.append(" (modified)")
print(str_line)
print(str_line.isEmpty)
str_line.append(".")
print(str_line)
print(str_line.starts(with: "Hello"))

//str_line.append("world.")
length = Int(str_line.count)
var lenghth_word = Int("world.".count)
let ind_w_size = str_line.index(str_line.startIndex, offsetBy: length - lenghth_word)
if "world." == str_line[ind_w_size..<str_line.endIndex]{
    print("This string line ends with 'world.' ")
} else {
    print("False. Ending of this string line is different.")
}

str_line.insert("-", at: str_line.index(after: String.Index(utf16Offset: 9, in: str_line)))
print(str_line)
str_line = str_line.replacingOccurrences(of: "Thus us", with: "It is")
print(str_line)

let st1 = str_line.index(str_line.startIndex, offsetBy: 10)
let st2 = str_line.index(str_line.startIndex, offsetBy: 15)

print(" 10 - th: ", str_line[st1], " 15 - th: ", str_line[st2])
let substring = str_line[st1..<st2]
print(substring)
