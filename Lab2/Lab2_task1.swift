


import Foundation
//1
var fibArray = [Int]()
for val in 0..<10{
    if val == 0 { fibArray.append(0) }
    if val == 1 { fibArray.append(1) }
    if val > 1 {
        fibArray.append(fibArray[val - 1] + fibArray[val - 2])
    }
}
print("fibArray : ", fibArray)

//2
var revArray = [Int]()
revArray = fibArray.reversed()
print("revArray : ", revArray)

//3
var snglArray = [Int]()

//4
for i in 2...100{
    for j in 2..<i{
        if fmod(Double(i), Double(j)) == 0{
            break
        } else {
            if i == j + 1{
                snglArray.append(i)
            }
        }
    }
}
print("snglArray : " ,snglArray)
print("Amount of elements in snglArray = ", snglArray.count)

//5
print("10th element of snglArray = ", snglArray[10])

//6
print("subarray from snglArray with elements from 15 to 20(by index) -> ", snglArray[15...20])

//7
var rptArray = [Int](repeating: snglArray[10], count: 10)
print("Repeated array is - ", rptArray)

//8 ... init
var oddArray = [Int8]()
for i:Int8 in 0..<10{
    if fmod(Double(i),2) != 0{
        oddArray.append(i)
    }
}
print("oddArray = ", oddArray)

//9..10
oddArray.append(11)
print("oddArray = ", oddArray)
oddArray += [15,17,19]
print("oddArray = ", oddArray)

//11
oddArray.insert(13, at: 6)   
print("oddArray = ", oddArray)

//12
oddArray.removeSubrange(5..<8)
print("oddArray = ", oddArray)

//13
print(oddArray.removeLast())
print("oddArray = ", oddArray)

//14
oddArray.replaceSubrange(2..<oddArray.endIndex, with: [2,3,4])
print("oddArray = ", oddArray)

//15
let indOfthree = oddArray.firstIndex(of: 3)
oddArray.remove(at: indOfthree!)
print("oddArray = ", oddArray)

//16
print("Is there an element, that equals '3' -> ", oddArray.contains(3))

//17
print(oddArray.description)