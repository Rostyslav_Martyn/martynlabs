


import Foundation

//1
let chSet: Set<Character> = ["a","b","c","d"]

//2
var mChSet : Set<Character> = chSet

//3
print("Amount of elements: ", mChSet.count)

//4
mChSet.insert("e")
mChSet.insert("f")
print(mChSet)

//5
var srtChSet = mChSet.sorted()   //??
print("Sorted srtChSet: ", srtChSet)

//6
var DeletedSign = mChSet.remove("f")
print("Deleted sign was: ", DeletedSign != nil ? DeletedSign! : "none")

//7
let indexd = mChSet.firstIndex(of: "d")
mChSet.remove(at: indexd!)
print("mChSet -> (string)", mChSet.description)

//8
let indexa = mChSet.firstIndex(of: "a")
print("Distance between first element and 'a' element: ", mChSet.distance(from: mChSet.startIndex, to: indexa!))

//9
mChSet.insert("a")
print("mChSet ->", mChSet)

//10
var aSet: Set<String> = ["One", "Two", "Three", "1", "2"]
var bSet: Set<String> = ["1", "2", "3", "One", "Two"]
print("aSet: ", aSet)
print("bSet: ", bSet)

//11
var abSet:Set<String> = aSet.intersection(bSet)
print("Similar elements from 2 sets: ", abSet)

//12
var uniqueAset: Set<String> = aSet.subtracting(bSet)
var uniqueBset: Set<String> = bSet.subtracting(aSet)
print("Unique elements from aSet -> ", uniqueAset)
print("Unique elements from bSet -> ", uniqueBset)

//13
var differenceSet: Set<String> = aSet.symmetricDifference(bSet)
print("Different elements in a and b sets", differenceSet)

//14
var unitedSet : Set<String> = aSet.union(bSet)
print("United set of a and b", unitedSet)

//15
var xSet : Set<Int> = [2,3,4]
var ySet : Set<Int> = [1,2,3,4,5,6]
var zSet : Set<Int> = [3,4,2]
var x1Set : Set<Int> = [5,6,7]
print(xSet,"\n", ySet, "\n", zSet, "\n" ,x1Set)

//16
print("Is ySet fully includes xSet: ", xSet.isSubset(of: ySet))
print("Is xSet fully includes ySet: ", ySet.isSubset(of: xSet))

//17
print("Is xSet fully includes ySet: ", xSet.isSuperset(of: ySet))
print("Is ySet fully includes xSet: ", ySet.isSuperset(of: xSet))

//18
if xSet == zSet {
    print("Sets are equal or ", true)
}else{
    print("Sets aren't equal or ", false)
}

//19
print("Is zSet fully includes xSet and not equal: ", xSet.isStrictSubset(of: zSet))

//20
print("Is xSet fully includes zSet and not equal: ", xSet.isStrictSuperset(of: zSet))