


import Foundation

//1
let nDict: [Int:String] = [ 1 : "One", 2 : "Two", 3 : "Three", 4 : "Four", 5 : "Five"]
print("Dictionary nDict is: ", nDict)

//2
print(nDict[3] != nil ? "Result: \(nDict[3]!)" : "No value for such key!")

//3
let dInd = nDict.index(nDict.startIndex, offsetBy: 4)
print("Value for index 4 -> ", nDict[dInd])

//4
var MNDict: [Int:String] = nDict
print("Dictionary MNDict is: ", MNDict)

//5
MNDict[6] = "Six"
MNDict.updateValue("Seven", forKey: 7)
print("Dictionary MNDict is: ", MNDict)

//6
MNDict.updateValue("Eight", forKey: 8)
print("Dictionary MNDict is: ", MNDict)

//7
MNDict.removeValue(forKey: 5)
print("Dictionary MNDict is: ", MNDict)

//8
let dInd_mn = MNDict.index(MNDict.startIndex, offsetBy: 4)
print("Removed value = ", MNDict.remove(at: dInd_mn))

//9
let ind1 = MNDict.index(forKey: 1)!
let ind7 = MNDict.index(forKey: 7)!
        
print("Distance between 1:One and 7:Seven elements in dictionary: ", MNDict.distance(from: ind1, to: ind7))

//10
var mndictKeys = [Int](MNDict.keys)
print("Keys from MNDict = ", mndictKeys)

//11
var mndictValues = [String](MNDict.values)
print("Values from MNDict = ", mndictValues)

//12
print("Amount of elements in MNDict : ", MNDict.count , "\n",
      "Amount of keys in MNDict : ", MNDict.keys.count, "\n",
      "Amount of values in MNDict : ", MNDict.values.count, "\n")

//13
print("MNDict in String view -> ", MNDict.description)