import Foundation

class Rational{
    var N:(m: Int,n: Int) = (0, 1)
    
    init(_ mi: Int,_ ni: Int){
        N.m = mi
        N.n = ni
    }
}

extension Rational{
    
    static func >(_ left: Rational,_ right: Rational) -> Bool{
        let temp1:Double = Double(left.N.m) / Double(left.N.n)
        let temp2:Double = Double(right.N.m) / Double(right.N.n)
        if temp1 > temp2{
            return true
        } else {
            return false
        }
    }
    static func <(_ left: Rational,_ right: Rational) -> Bool{
        let temp1:Double = Double(left.N.m) / Double(left.N.n)
        let temp2:Double = Double(right.N.m) / Double(right.N.n)
        if temp1 < temp2{
            return true
        } else {
            return false
        }
    }
    static func ==(_ left: Rational,_ right: Rational) -> Bool{
        let temp1:Double = Double(left.N.m) / Double(left.N.n)
        let temp2:Double = Double(right.N.m) / Double(right.N.n)
        if temp1 == temp2{
            return true
        } else {
            return false
        }
    }
    
    static func +(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
        if left.N.n == right.N.n{
            res.N.m = left.N.m + right.N.m
            res.N.n = left.N.n
        } else {
            res.N.m = left.N.m * right.N.n + right.N.m * left.N.n
            res.N.n = left.N.n * right.N.n
        }
        return res
    }
    static func -(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
        if left.N.n == right.N.n{
            res.N.m = left.N.m - right.N.m
            res.N.n = left.N.n
        } else {
            res.N.m = left.N.m * right.N.n - right.N.m * left.N.n
            res.N.n = left.N.n * right.N.n
        }
        return res
    }
    static func *(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
            res.N.m = left.N.m * right.N.m
            res.N.n = left.N.n * right.N.n
        return res
    }
    static func /(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
            res.N.m = left.N.m * right.N.n
            res.N.n = left.N.n * right.N.m
        return res
    }
    
}

class Calculator {
    static let operations = Calculator()
    
    static func plus(_ n1: Rational,_ n2: Rational) -> Rational{
        let res = n1 + n2
        return res
    }
    static func minus(_ n1: Rational,_ n2: Rational) -> Rational{
        let res = n1 - n2
        return res
    }
    func mult(_ n1: Rational,_ n2: Rational) -> Rational{
        let res = n1 * n2
        return res
    }
    func div(_ n1: Rational,_ n2: Rational) -> Rational{
        let res = n1 / n2
        return res
    }
    func copy(_ n: Rational) -> Rational{
        let R = Rational(n.N.m, n.N.n)
        return R
    }
    
    private init() { }
}


var a = Rational(2, 4)
var b = Rational(3, 5)
var c = Rational(-4, 8)

print(Calculator.plus(a, b).N)
print(Calculator.minus(a, b).N)
print(Calculator.operations.mult(a, b).N)
print(Calculator.operations.div(a, b).N)


print(a.N, " > ", b.N, ": ", a > b )
print(b.N, " > ", a.N, ": ", b > a )
print(a.N, " < ", b.N, ": ", a < b )
print(c.N, " < ", a.N, ": ", c < a )
print(b.N, " < ", a.N, ": ", b < a )
print(a.N, " == ", b.N, ": ", a == b )

var testR = Calculator.operations.copy(a)
print("TestR: ", testR.N )