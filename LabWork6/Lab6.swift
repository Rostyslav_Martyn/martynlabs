import Foundation
extension CartesianPoint{
    func StringView() -> String{
        var view: String
        view = "Декартові координати:  x: " + String(format: "%.4f", self.SetGetX) + " y: " + String(format: "%.4f", self.SetGetY)
        return view
    }
}

extension PolarPoint{
    func StringView() -> String{
        var view: String
        view = "Полярні координати: радіус: " + String(format: "%.4f", self.SetGetRad) + "градуси: " + String(format: "%.4f", self.SetGetDeg)
        return view
    }
}
extension Figure{
    
    var PropPerimentr: Double{
        var FPerimetr: Double = 0
        if type == .line || type == .point || type == .undified{
            print("У фігури немає цих даних")
            return -1
        }else {
            for i in 0..<SetGetOrigin.count{
                if i == SetGetOrigin.count - 1{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[0])
                }else{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[i + 1])
                }
            }
        }
        return FPerimetr
    }
    
    var PropSqure: Double{
        var FSquare:Double = 0
        if type == .line || type == .point{
             print("У фігури немає цих даних")
            return -1
        } else {
            if type == .triangle{
                let a = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[1])
                let b = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[2])
                let c = distance(Point1: SetGetOrigin[2], Point2: SetGetOrigin[0])
                let p = (a + b + c)/2
                FSquare = sqrt(p * (p - a) * (p - b) * (p - c))
            }
            if type == .quadrilateral{
                let d = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[2])
                let D = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[3])
                FSquare = (D * d)/2
            }
            if type == .polygon{
                let halfIndex:Int = (SetGetOrigin.count / 2)
                let temp = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[halfIndex])
                FSquare = 1/2 * self.Perimetr() * temp
            }
        }
        
        return FSquare
    }
    
    func Perimetr() -> Double{
        var FPerimetr: Double = 0
        if type == .line || type == .point || type == .undified{
            print("У фігури немає цих даних")
            return -1
        }else {
            for i in 0..<SetGetOrigin.count{
                if i == SetGetOrigin.count - 1{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[0])
                }else{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[i + 1])
                }
            }
        }
        return FPerimetr
    }
    
    func Square() -> Double{
        var FSquare:Double = 0
        if type == .line || type == .point{
             print("У фігури немає цих даних")
            return -1
        } else {
            if type == .triangle{
                let a = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[1])
                let b = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[2])
                let c = distance(Point1: SetGetOrigin[2], Point2: SetGetOrigin[0])
                let p = (a + b + c)/2
                FSquare = sqrt(p * (p - a) * (p - b) * (p - c))
            }
            if type == .quadrilateral{
                let d = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[2])
                let D = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[3])
                FSquare = (D * d)/2
            }
            if type == .polygon{
                let halfIndex:Int = (SetGetOrigin.count / 2)
                let temp = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[halfIndex])
                FSquare = 1/2 * self.Perimetr() * temp
            }
        }
        
        return FSquare
    }
    
}
extension PolarPoint{
    func distance(Point1: PolarPoint, Point2: PolarPoint) -> Double{
        let x_t1 = Point1.SetGetRad * cos(Point1.SetGetDeg)
        let y_t1 = Point1.SetGetRad * sin(Point1.SetGetDeg)
        let x_t2 = Point2.SetGetRad * cos(Point2.SetGetDeg)
        let y_t2 = Point2.SetGetRad * sin(Point2.SetGetDeg)

        let d = sqrt(pow(x_t2 - x_t1, 2) + pow(y_t2 - y_t1, 2))
        return d
    }
}
extension CartesianPoint{
    func distance(Point1: CartesianPoint, Point2: CartesianPoint) -> Double{
        let d = sqrt(pow(Point2.SetGetX - Point1.SetGetX, 2) + pow(Point2.SetGetY - Point1.SetGetY, 2))
        return d
    }
}
func distance(Point1: CartesianPoint, Point2: CartesianPoint) -> Double{
    let d = sqrt(pow(Point2.SetGetX - Point1.SetGetX, 2) + pow(Point2.SetGetY - Point1.SetGetY, 2))
    return d
}
func distance(Point1: CartesianPoint, Point2: PolarPoint) -> Double{
    
    let x_t = Point2.SetGetRad * cos(Point2.SetGetDeg)
    let y_t = Point2.SetGetRad * sin(Point2.SetGetDeg)
    
    let d = sqrt(pow(x_t - Point1.SetGetX, 2) + pow(y_t - Point1.SetGetY, 2))
    return d
}
func distance(Point1: PolarPoint, Point2: CartesianPoint) -> Double{
    
    let x_t = Point1.SetGetRad * cos(Point1.SetGetDeg)
    let y_t = Point1.SetGetRad * sin(Point1.SetGetDeg)
    
    let d = sqrt(pow(Point2.SetGetX - x_t, 2) + pow(Point2.SetGetY - y_t, 2))
    return d
}

struct CartesianPoint{
    private var x = 0.0
    private var y = 0.0
    
    var SetGetX:Double{
        get{
            return x
        }
        set(newX){
            x = newX
        }
    }
    var SetGetY:Double{
        get{
            return y
        }
        set(newY){
            y = newY
        }
    }
    
    init() {}
    init(x: Double, y:Double) {
        self.x = x
        self.y = y
    }
    init(fromPolarCoord radius:Double, degree: Double) {
        x = radius * cos(degree)
        y = radius * sin(degree)
    }
}

struct PolarPoint{
    var radius = 0.0
    var SetGetRad:Double{
        get{
            return radius
        }
        set(newRadius){
            radius = newRadius
        }
    }
    var degree = 0.0
    var SetGetDeg:Double{
        get{
            return degree
        }
        set(newDegree){
            degree = newDegree
        }
    }
    init() {}
    init(radius: Double, degree:Double) {
        self.radius = radius
        self.degree = degree
    }
    init(fromCartesianCoord x: Double, y: Double){
        radius = sqrt(pow(x,2) + pow(y, 2))
        let tmp = x / radius
        degree = acos(tmp)  
    }
}

struct Figure{
    private var origin = [CartesianPoint]()
    enum FType {
        case undified, triangle, quadrilateral, polygon, point, line
    }
    var SetGetOrigin: [CartesianPoint]{
        set(newOrigin){
            origin = newOrigin
        }
        get{
            return origin
        }
    }
    var type: FType{
        if origin.count == 1 {
            return .point
        }
        if origin.count == 2{
            if origin[0].SetGetX != origin[1].SetGetX || origin[0].SetGetY != origin[1].SetGetY{
                return .line
            }
        }
        if origin.count == 3 {
            if (origin[0].SetGetX != origin[1].SetGetX || origin[0].SetGetY != origin[1].SetGetY) && (origin[2].SetGetX != origin[0].SetGetX || origin[2].SetGetY != origin[0].SetGetY) {
                return .triangle
            }
        }
        if origin.count == 4 {
            return .quadrilateral
            }
        if origin.count >= 5{
            return .polygon
        }
        return .undified
    }
    
    init(fromCartesianPointsArray array: [CartesianPoint]) {
        origin = array;
    }
    
    subscript(index: Int) -> CartesianPoint?{
        if origin.count < index{
            return nil
        }
        return origin[index]
    }
    
}

func PointDescription(point: CartesianPoint){
    print("X: ", String(format: "%.4f", point.SetGetX), "Y: ", String(format: "%.4f", point.SetGetY))
}
func PointDescription(point: PolarPoint){
    print("Радіус: ", String(format: "%.4f", point.SetGetRad), " Градуси: ", String(format: "%.4f", point.SetGetDeg))
}

var PointC1 = CartesianPoint(x: 4.1, y: 6.3)
print("X: ",PointC1.SetGetX, "Y: ", PointC1.SetGetY)
PointC1.SetGetX = 10 ; PointC1.SetGetY = 5
print("X: ", PointC1.SetGetX, "Y: ", PointC1.SetGetY)

var PointC2 = CartesianPoint(fromPolarCoord: 6.4, degree: 0.896)
print("X: ", String(format: "%.3f", PointC2.SetGetX), "Y: ", String(format: "%.3f", PointC2.SetGetY))


var PointP1 = PolarPoint.init(fromCartesianCoord: 8, y: 9)
print("Радіус: ", String(format: " %.2f", PointP1.SetGetRad)," Градуси: ", String(format: " %.4f", PointP1.SetGetDeg))

PointC2.SetGetX = 5
PointC2.SetGetY = 10
print("X: ", PointC2.SetGetX, "Y: ", PointC2.SetGetY)

PointDescription(point: PointC1)
PointDescription(point: PointP1)

var PointP2 = PolarPoint(radius: 3.6, degree: 0.755)

print("\n")
print("Дистанція між P1 і P2 = ", String(format: "%.4f", PointP1.distance(Point1: PointP1, Point2: PointP2)))
print("Дистанція між P1 і С1 =  ", String(format: "%.4f", distance(Point1: PointP1, Point2: PointC1)))
print("Дистанція між С2 і P2 =  ", String(format: "%.4f", distance(Point1: PointC2, Point2: PointP2)))
print("Дистанція між С2 і С1 =  ", String(format: "%.4f", PointC1.distance(Point1: PointC1, Point2: PointC2)))

print(PointC1.StringView())
print(PointP1.StringView())

var PointC3 = CartesianPoint(x: 2.13, y: 3.24)
var PointC4 = CartesianPoint(x: 4.27, y: 15.24)
var PointC5 = CartesianPoint(x: 3.12, y: 10.03)
var PointC6 = CartesianPoint(x: 1.51, y: 3.50)
var PointC7 = CartesianPoint(x: 13.26, y: 2.16)
var PointC8 = CartesianPoint(x: 10.00, y: 8.39)

var TParray:[CartesianPoint] = [PointC1, PointC2, PointC3]
var triangle1 = Figure(fromCartesianPointsArray: TParray)

print("Фігура: ", triangle1.type)

TParray.removeLast()
var line1 = Figure(fromCartesianPointsArray: TParray)
print("Фігура: ", line1.type)

TParray.append(PointC4)
TParray.append(PointC5)

var quad = Figure(fromCartesianPointsArray: TParray)
print("Фігура:", quad.type)

TParray.append(PointC6)
TParray.append(PointC7)

var poly = Figure(fromCartesianPointsArray: TParray)
print("Фігура: ", poly.type)


print("Периметр triangle = ", String(format: "%.4f", triangle1.Perimetr()))
print("Периметр quad = ", String(format: "%.4f", quad.Perimetr()))
print("Периметр quad property = ", String(format: "%.4f", quad.PropPerimentr))

print("Площа quad = ", String(format: "%.4f", quad.Square()))
print("Площа quad property = ", String(format: "%.4f", quad.PropSqure))
print("Площа poly property = ", String(format: "%.4f", poly.PropSqure))